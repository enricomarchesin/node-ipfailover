// https://github.com/diosney/node-iproute
var ip_link=require('iproute').link; 
var ip_route = require('iproute').route;
// https://github.com/danielzzz/node-ping
var ping = require('ping');
var request = require('request');


var config = require('./config.json');
var primaryDevice=config.primary.dev||"eth0",primaryGateway=config.primary.gw;
var backupDevice=config.backup.dev||"eth1",backupGateway=config.backup.gw;
var testHost=config.test||"8.8.4.4";
var DEBUG=config.debug||false;

var SERVER_URL=config.server||'http://lelidrop.net';
var MACHINE_ID=config.machine||0;

function msg(message) {
	console.log('['+new Date().toISOString()+']',message);
}

function switchtoPrimary() {
	ip_route.replace({
		to: 'default',
		via: primaryGateway,
		dev: primaryDevice
	}, function (error) {
		if (error) {
			msg('cannot switch to primary connection!');
			console.log(error);
		} else {
			msg('switched to primary connection...');
			setTimeout(function(){
				request(SERVER_URL+'?m='+MACHINE_ID+'&n=18&v=0&x=', function (error, response, body) {
					if (!error && response.statusCode == 200) {
						msg('remote server notified');
					} else {
						msg('cannot notify remote server!');
					}
				});
			},10000);
		}
	});
}
function switchtoBackup() {
	ip_route.replace({
		to: 'default',
		via: backupGateway,
		dev: backupDevice
	}, function (error) {
		if (error) {
			msg('cannot switch to backup connection!');
			console.log(error);
		} else {
			msg('switched to backup connection...');
			setTimeout(function(){
				request(SERVER_URL+'?m='+MACHINE_ID+'&n=18&v=1&x=', function (error, response, body) {
					if (!error && response.statusCode == 200) {
						msg('remote server notified');
					} else {
						msg('cannot notify remote server!');
					}
				});
			},10000);
		}
	});
}

function checkDefaultRoute(routes,primary,backup) {
	var onBackup=false;
	backupRoutes=routes.filter(function(route){
		return route.type=='unicast'&&route.to=='default'&&route.dev==backup.name;
	});
	if (backupRoutes.length==0 && primary.state=='UP') {
		if (DEBUG) msg('on primary connection');
		ping.sys.probe(testHost,function(isAlive) {
			if (isAlive) {
				if (DEBUG) msg('primary connection still available...');
			} else {
				msg('primary connection not available anymore!');
				switchtoBackup();
			}
		});
	} else {
		if (DEBUG) msg('on backup connection');
		if (primary.state=='UP') {
			ping.sys.probe(testHost,function(isAlive) {
				if (isAlive) {
					msg('primary connection restored');
					switchtoPrimary();
				} else {
					if (DEBUG) msg('primary connection still unavailable...');
				}
			});
		} else {
			if (DEBUG) msg('primary connection still disconnected...');
		}
	}
}

function checkRoutes(primary,backup) {
	ip_route.show(function (error, routes) {
		if (error) {
			msg('cannot get ip routes!');
			console.log(error);
		} else {
			var staticRoute;
			staticRoutes=routes.filter(function(route){
				return route.type=='unicast'&&route.to=='8.8.4.4'&&route.dev==primary.name;
			});
			if (staticRoutes.length==0) {
				ip_route.add({
					to: testHost,
					via: primaryGateway,
					dev: primaryDevice
				}, function (error) {
					if (error) {
						console.log(error);
					} else {
						msg('added '+primaryDevice+' check (static) route');
					}
					checkDefaultRoute(routes,primary,backup);
				});
			} else {
				if (DEBUG) msg('no need to add '+primaryDevice+' check (static) route');
				checkDefaultRoute(routes,primary,backup);
			}
		}
	});
}

function checkDevices() {
	ip_link.show(function (error, links) {
		if (error) {
			console.log(error);
		} else {
			if (links.length>0) {
				var primaryLink,backupLink;

				links.forEach(function(link){
					if (!primaryLink && link.name==primaryDevice) primaryLink=link;
					else if (!backupLink && link.name==backupDevice) backupLink=link;
				});

				if (primaryLink) {
					if (DEBUG) msg('primary: '+primaryLink.name+' ('+primaryLink.state+')');
					//console.log(primaryLink);
					if (backupLink) {
						if (DEBUG) msg('backup: '+backupLink.name+' ('+backupLink.state+')');
						//console.log(backupLink);
						checkRoutes(primaryLink,backupLink);
					} else {
						msg('no backup iface ("'+backupDevice+'") present: no need to proceed...');
					}
				} else {
					msg('no primary iface ("'+primaryDevice+'") present: something REALLY wrong!');
				}
			} else {
				msg('no known iface ('+ifaces+')!');
			}
		}
	});
}

checkDevices();
setInterval(checkDevices,10*1000);
