#!/bin/sh

#kill all pending instances
sudo kill `pgrep nodejs`
sleep 1

#starting ip failover checks
cd ~/ipfailover
sudo nodejs check.js
